'use client';

import React from 'react';
import { Drawer as MUIDrawer } from '@mui/material';

type DrawerSortingProps = {
  direction: 'right' | 'left' | 'top' | 'bottom';
  open: boolean;
  onClose: () => void;
  children: React.ReactNode;
};

export const Drawer = ({
  direction,
  open,
  onClose,
  children,
}: DrawerSortingProps): React.ReactElement => {
  return (
    <MUIDrawer anchor={direction} open={open} onClose={onClose}>
      {children}
    </MUIDrawer>
  );
};
