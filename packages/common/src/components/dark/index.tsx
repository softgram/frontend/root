'use client';

import React from 'react';
import { ThemeProvider, createTheme } from '@mui/material';

type DarkProps = {
  children: React.ReactNode;
};

export const Dark = ({ children }: DarkProps): React.ReactElement => {
  return (
    <ThemeProvider
      theme={createTheme({
        palette: {
          mode: 'dark',
        },
      })}
    >
      {children}
    </ThemeProvider>
  );
};
