'use client';

import React, { Fragment, useRef } from 'react';
import {
  Select as MUISelect,
  FormControl,
  InputLabel,
  MenuItem,
  SelectChangeEvent,
} from '@mui/material';
import { useFormContext, Controller } from 'react-hook-form';
import { v4 as uuid } from 'uuid';

type Item = {
  label: string;
  value: string | number | Array<string>;
  default?: boolean;
};

type SelectProps = {
  className?: string;
  name: string;
  label?: string;
  data: Array<Item>;
  variant?: 'filled' | 'standard' | 'outlined';
  onChange?: (event: SelectChangeEvent<any>) => void;
  color?: 'error' | 'primary' | 'secondary' | 'info' | 'success' | 'warning';
};

export const Select = ({
  className,
  name,
  label,
  data,
  variant = 'outlined',
  onChange,
  color,
}: SelectProps): React.ReactElement => {
  const { control } = useFormContext();
  const ref = useRef(label?.concat(uuid()));

  return (
    <div className={`w-full ${className}`}>
      <Controller
        render={({
          field: { onChange: changeForm, value },
          fieldState: { error },
        }) => (
          <Fragment>
            <FormControl fullWidth error={!!error}>
              <InputLabel
                color={color}
                id={ref.current}
                size="small"
                sx={{ fontSize: '13px' }}
              >
                {label}
              </InputLabel>
              <MUISelect
                variant={variant}
                color={color}
                onChange={(event) => {
                  if (onChange) {
                    onChange(event);
                  }

                  changeForm(event);
                }}
                value={value ?? ''}
                error={!!error}
                labelId={ref.current}
                label={label}
                size={'small'}
                slotProps={{
                  input: {
                    style: {
                      fontSize: '13px',
                    },
                  },
                  root: {
                    style: {
                      fontSize: '13px',
                    },
                  },
                }}
              >
                {data.map((option) => (
                  <MenuItem
                    key={option.label}
                    value={option.value}
                    sx={{
                      fontSize: '13px',
                    }}
                    selected={option.default}
                  >
                    {option.label}
                  </MenuItem>
                ))}
              </MUISelect>
            </FormControl>
            <span
              className={`text-[11px] font-bold ${
                error ? 'text-red-600' : 'text-transparent'
              }`}
            >
              {error?.message ?? '--'}
            </span>
          </Fragment>
        )}
        name={name}
        control={control}
      />
    </div>
  );
};
