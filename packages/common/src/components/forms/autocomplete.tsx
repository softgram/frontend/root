'use client';

import React, { CSSProperties, useEffect, useMemo } from 'react';
import {
  Autocomplete as MUIAutocomplete,
  AutocompleteOwnerState,
  AutocompleteRenderGetTagProps,
  Box,
  FormControl,
  IconButtonProps,
  TextField,
} from '@mui/material';
import { useFormContext } from 'react-hook-form';
import _ from 'lodash';

type AutocompleteProps<T extends Record<string, unknown>> = {
  name: string;
  multiple?: boolean;
  data: Array<T>;
  classNames?: {
    container: string;
    option: string;
  };
  variant?: 'filled' | 'outlined' | 'standard';
  color?: 'primary' | 'secondary' | 'success' | 'error' | 'info' | 'warning';
  onKeyDown?: (e: React.KeyboardEvent<HTMLDivElement>) => void;
  expresion: string;
  label: string;
  expresionArray?: {
    expresions: Array<{
      value: string;
      className?: string;
      style?: CSSProperties;
    }>;
    separator: string;
  };
  keyExpresion?: string;
  keyExpresionArray?: Array<string>;
  onChange?: (value: object | object[] | null, methods: any) => void;
  ListBoxComponent?:
    | React.JSXElementConstructor<React.HTMLAttributes<HTMLElement>>
    | undefined;
  renderTags?:
    | ((
        value: object[],
        getTagProps: AutocompleteRenderGetTagProps,
        ownerState: AutocompleteOwnerState<
          object,
          boolean,
          false,
          false,
          'div'
        >,
      ) => React.ReactNode)
    | undefined;
  value?: object | object[] | null;
  clearIndicator?: Partial<IconButtonProps<'button', {}>> | undefined;
};

export const Autocomplete = <T extends Record<string, unknown>>({
  name,
  expresionArray,
  multiple,
  data,
  classNames,
  variant = 'standard',
  color,
  onKeyDown,
  label,
  expresion,
  keyExpresion,
  onChange,
  ListBoxComponent,
  renderTags,
  value,
  clearIndicator,
  keyExpresionArray,
}: AutocompleteProps<T>): React.ReactElement => {
  const {
    setValue,
    getFieldState,
    formState: { errors },
    clearErrors,
  } = useFormContext();

  const { error } = useMemo(() => getFieldState(name), [value, errors]);

  useEffect(() => {
    setValue(name, value);
  }, [value]);

  return (
    <div className={`w-full ${classNames?.container}`}>
      <MUIAutocomplete<object, boolean>
        filterOptions={(options, { inputValue }) => {
          return options.filter(
            (option) =>
              _.get(option, expresion)
                .toString()
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) !== -1,
          );
        }}
        value={value ?? []}
        renderTags={renderTags}
        ListboxComponent={ListBoxComponent}
        options={data}
        size="small"
        onChange={(_, newValue) => {
          if (onChange) {
            onChange(newValue, clearErrors);
          }
          setValue(name, newValue);
        }}
        renderOption={(props, option) => {
          let key = '';

          if (keyExpresionArray) {
            key = keyExpresionArray
              .map((keySimple) => _.get(option, keySimple))
              .join('');
          } else if (keyExpresion) {
            key = _.get(option, keyExpresion);
          } else {
            key = _.get(option, expresion);
          }

          if (expresionArray) {
            return (
              <Box component="li" {...props} key={key}>
                <span className={classNames?.option}>
                  {expresionArray.expresions.map((expresionSimple, index) => (
                    <span
                      className={`${classNames?.option} ${expresionSimple.className}`}
                      key={index}
                      style={expresionSimple.style}
                    >
                      {`${_.get(option, expresionSimple.value)}${
                        expresionArray.separator
                      }`}
                    </span>
                  ))}
                </span>
              </Box>
            );
          }

          return (
            <Box component="li" {...props} key={key}>
              <span className={classNames?.option}>
                {_.get(option, expresion)}
              </span>
            </Box>
          );
        }}
        getOptionLabel={(option) => {
          return _.get(option, expresion);
        }}
        slotProps={{
          paper: {
            sx: {
              fontSize: '13px',
              maxHeight: '12rem',
              '&::-webkit-scrollbar': {
                display: 'none',
              },
            },
          },
          popper: {
            sx: {
              overflow: 'scroll',
              fontSize: '13px',
            },
            className: 'scroll-none',
          },
          clearIndicator,
        }}
        ChipProps={{
          size: 'small',
          sx: {
            height: '1.5rem',
            fontSize: '12px',
          },
        }}
        multiple={multiple}
        renderInput={(params) => {
          const paramsProps = {
            ...params,
            InputProps: {
              ...params.InputProps,
              sx: {
                fontSize: '13px',
              },
            },
            InputLabelProps: {
              ...params.InputLabelProps,
              sx: {
                fontSize: '13px',
              },
            },
          };

          return (
            <FormControl error={!!error} className="w-full">
              <TextField
                {...paramsProps}
                label={label}
                size="small"
                variant={variant}
                color={color}
                onKeyDown={onKeyDown}
                error={!!error}
              />
            </FormControl>
          );
        }}
      />
    </div>
  );
};
