'use client';

import React from 'react';
import { AnyObjectSchema } from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import {
  FormProvider,
  UseFormReturn,
  useForm,
  SubmitHandler,
  DefaultValues,
} from 'react-hook-form';

type FormProps<T extends Record<string, unknown>> = {
  children:
    | React.ReactNode
    | ((data: UseFormReturn<T, unknown>) => React.ReactNode);
  onSubmit: SubmitHandler<T>;
  className?: string;
  schema?: AnyObjectSchema;
  defaultValues?: DefaultValues<T> | undefined;
};

const Form = <T extends Record<string, unknown>>({
  children,
  onSubmit,
  className,
  schema,
  defaultValues,
}: FormProps<T>): React.ReactElement => {
  const methods = useForm<T>({
    resolver: schema ? yupResolver(schema) : undefined,
    defaultValues,
  });

  return (
    <FormProvider {...methods}>
      <form
        onSubmit={methods.handleSubmit(onSubmit)}
        className={className}
        autoComplete="off"
      >
        {typeof children === 'function' ? children(methods) : children}
      </form>
    </FormProvider>
  );
};

export { Form };
export type { FormProps };
