'use client';

import { FormControl, SxProps, TextField } from '@mui/material';
import React, { Fragment } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

type InputProps = {
  label?: string;
  variant?: 'standard' | 'filled' | 'outlined';
  type?: string;
  name: string;
  className?: string;
  InputProps?: Record<string, unknown>;
  InputLabelProps?: Record<string, unknown>;
  startAdornment?: React.ReactNode;
  sx?: SxProps;
  color?: 'primary' | 'secondary' | 'success' | 'error' | 'info' | 'warning';
  onKeyDown?: (e: React.KeyboardEvent<HTMLDivElement>) => void;
  onChangeMis?: (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => void;
};

const Input = ({
  label,
  variant = 'standard',
  type,
  name,
  className,
  sx,
  InputProps,
  InputLabelProps,
  color,
  onKeyDown,
  startAdornment,
  onChangeMis,
}: InputProps): React.ReactElement => {
  const { control } = useFormContext();

  return (
    <div className={`w-full ${className ? className : ''}`}>
      <Controller
        name={name}
        control={control}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Fragment>
            <FormControl error={!!error} className="w-full">
              <TextField
                label={label}
                variant={variant}
                type={type}
                onChange={(event) => {
                  onChangeMis && onChangeMis(event);

                  onChange(event);
                }}
                color={color}
                value={value ?? ''}
                error={!!error}
                className="w-full"
                size="small"
                onKeyDown={onKeyDown}
                sx={sx}
                InputProps={{
                  style: {
                    fontSize: '13px',
                  },
                  startAdornment,
                  ...InputProps,
                }}
                InputLabelProps={{
                  style: {
                    fontSize: '13px',
                  },
                  ...InputLabelProps,
                }}
              />
            </FormControl>
            <span
              className={`text-[11px] font-bold ${
                error ? 'text-red-600' : 'text-transparent'
              }`}
            >
              {error?.message ?? '--'}
            </span>
          </Fragment>
        )}
      />
    </div>
  );
};

export { Input };
export type { InputProps };
