'use client';

import React from 'react';
import { Button as MUButton, SxProps } from '@mui/material';

type ButtonProps = Partial<{
  children: React.ReactNode;
  onClick: React.MouseEventHandler<HTMLButtonElement> | undefined;
  disabled: boolean;
  color:
    | 'inherit'
    | 'primary'
    | 'secondary'
    | 'success'
    | 'error'
    | 'info'
    | 'warning'
    | undefined;
  variant: 'text' | 'contained' | 'outlined' | undefined;
  className: string;
  sx: SxProps;
  type: 'submit' | 'button';
  startIcon: React.ReactNode;
  endIcon: React.ReactNode;
  ariaLabel: string;
  error?: boolean;
}>;

const Button = ({
  children,
  onClick,
  disabled,
  color = 'primary',
  variant = 'outlined',
  className,
  sx,
  type = 'submit',
  startIcon,
  endIcon,
  ariaLabel,
  error,
}: ButtonProps): React.ReactElement => {
  return (
    <div className={`w-full ${className ?? ''}`}>
      <MUButton
        className={`w-full`}
        variant={variant}
        color={error ? 'error' : color}
        onClick={onClick}
        disabled={disabled}
        type={type}
        startIcon={startIcon}
        endIcon={endIcon}
        sx={sx}
        aria-label={ariaLabel}
      >
        {children}
      </MUButton>
    </div>
  );
};

export { Button };
export type { ButtonProps };
