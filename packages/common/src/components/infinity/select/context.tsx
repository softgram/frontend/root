'use client';

import { createContext, useState, useContext } from 'react';

type ContextProps = {
  open: HTMLButtonElement | null;
  setOpen: React.Dispatch<React.SetStateAction<HTMLButtonElement | null>>;
  selected: object | null;
  setSelected: React.Dispatch<React.SetStateAction<object | null>>;
};

const context = createContext<ContextProps | null>(null);

const ContextProvider = ({
  children,
}: {
  children: React.ReactElement;
}): React.ReactElement => {
  const [open, setOpen] = useState<HTMLButtonElement | null>(null);
  const [selected, setSelected] = useState<object | null>(null);

  return (
    <context.Provider value={{ open, setOpen, selected, setSelected }}>
      {children}
    </context.Provider>
  );
};

const useContextSelect = () => {
  const data = useContext(context);

  if (data === null) {
    throw new Error('Error exception here selected');
  }

  return data;
};

export { ContextProvider, useContextSelect };
