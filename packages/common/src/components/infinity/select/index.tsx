'use client';

import { CircularProgress, MenuItem } from '@mui/material';
import {
  FetchNextPageOptions,
  InfiniteData,
  InfiniteQueryObserverResult,
} from '@tanstack/react-query';
import React from 'react';
import { useInView } from 'react-intersection-observer';
import { v4 as uuid } from 'uuid';
import _ from 'lodash';
import { useFormContext } from 'react-hook-form';
import { ContextProvider, useContextSelect } from './context';
import { Popover, PopoverProps } from './popover';

type ItemSelect = {
  onClick: () => void;
  item: object;
  expresion: string;
  index: number;
  delay: number;
  className?: string;
};

const ItemSelect = ({
  onClick,
  item,
  expresion,
  className = 'text-indigo-600',
}: ItemSelect): React.ReactElement => {
  return (
    <MenuItem
      color="green"
      className="flex h-full w-full cursor-pointer items-center justify-center bg-white duration-200 hover:bg-gray-300"
      onClick={onClick}
      key={uuid()}
    >
      <p className={'text-xs font-bold ' + className}>
        {_.get(item, expresion)}
      </p>
    </MenuItem>
  );
};

type InfinitySelectComponentType<T extends Record<string, unknown>> = {
  data?: InfiniteData<Array<T>>;
  fetchNextPage: (
    options?: FetchNextPageOptions | undefined,
  ) => Promise<InfiniteQueryObserverResult<any, unknown>>;
  expresion: string;
  name: string;
  popoverProps?: PopoverProps;
  hasNextPage: boolean;
  classNames?: Partial<{
    container: string;
    option: string;
  }>;
};

const InfinitySelectComponent = <T extends Record<string, unknown>>({
  data,
  fetchNextPage,
  expresion,
  name,
  popoverProps,
  hasNextPage,
  classNames,
}: InfinitySelectComponentType<T>): React.ReactElement => {
  const { setValue } = useFormContext();
  const { setOpen, setSelected } = useContextSelect();
  const { ref, entry } = useInView({
    threshold: 0,
  });

  const pages = React.useMemo(
    () => data?.pages.reduce((prevPages, page) => prevPages.concat(page)),
    [data],
  );

  if (entry?.isIntersecting && hasNextPage) {
    fetchNextPage();
  }

  return (
    <div className={`w-full ${classNames?.container}`}>
      <Popover {...popoverProps} expresion={expresion}>
        {pages?.map((item: any, index: number) => (
          <ItemSelect
            className={classNames?.option}
            item={item}
            expresion={expresion}
            index={index}
            delay={0.2}
            onClick={() => {
              setOpen(null);
              setSelected(item);
              setValue(name, item);
            }}
            key={index}
          />
        ))}
        {hasNextPage && (
          <div className="flex h-8 items-center justify-center" ref={ref}>
            <CircularProgress size={'20px'} />
          </div>
        )}
      </Popover>
    </div>
  );
};

export const InfinitySelect = <T extends Record<string, unknown>>({
  data,
  fetchNextPage,
  expresion,
  name,
  popoverProps,
  hasNextPage,
  classNames,
}: InfinitySelectComponentType<T>): React.ReactElement => {
  return (
    <ContextProvider>
      <InfinitySelectComponent<T>
        fetchNextPage={fetchNextPage}
        hasNextPage={hasNextPage}
        data={data}
        expresion={expresion}
        name={name}
        classNames={classNames}
        popoverProps={popoverProps}
      />
    </ContextProvider>
  );
};
