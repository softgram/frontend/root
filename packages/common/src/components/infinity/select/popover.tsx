'use client';

import { PaperProps, Popover as MUIPopover } from '@mui/material';
import { Button } from '../../forms/button';
import React from 'react';
import _ from 'lodash';
import { useContextSelect } from './context';
import { ButtonProps } from '../../forms/button';

type PopoverProps = Partial<{
  className: string;
  buttonProps: ButtonProps;
  children: React.ReactNode;
  popoverProps: Partial<PaperProps<'div', {}>> | undefined;
  text: string;
  expresion: string;
}>;

const Popover = ({
  children,
  text,
  buttonProps,
  className,
  popoverProps,
  expresion,
}: PopoverProps) => {
  const { open, setOpen, selected } = useContextSelect();
  const openData = Boolean(open);
  const id = open ? 'simple-popover' : undefined;

  return (
    <div>
      <Button
        aria-describedby={id}
        variant="outlined"
        type="button"
        onClick={(event) => setOpen(event.currentTarget)}
        {...buttonProps}
      >
        <span className={className}>
          {selected ? _.get(selected, expresion ?? '') : text}
        </span>
      </Button>
      <MUIPopover
        id={id}
        open={openData}
        anchorEl={open}
        onClose={() => setOpen(null)}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        PaperProps={{
          style: {
            width: '7rem',
            maxHeight: '200px',
          },
          className: 'scroll-none',
          ...popoverProps,
        }}
      >
        {children}
      </MUIPopover>
    </div>
  );
};

export { Popover };
export type { PopoverProps };
