'use client';

import {
  TableBody as MUITableBody,
  TableRow,
  TableCell as MUITableCell,
  CircularProgress,
} from '@mui/material';
import { Column } from '.';
import _ from 'lodash';

import { v4 as uuid } from 'uuid';
import { Fragment } from 'react';

const TableCell = <T extends unknown>({
  field,
  column,
}: {
  field: T;
  column: Column<T>;
}) => {
  return (
    <MUITableCell>
      {column.component ? (
        <column.component
          data={
            column.expresion === 'all' ? '' : _.get(field, column.expresion)
          }
          all={
            column.expresion === 'all' ? field : _.get(field, column.expresion)
          }
        ></column.component>
      ) : (
        <div className={column.className}>{_.get(field, column.expresion)}</div>
      )}
    </MUITableCell>
  );
};

type TableBodyProps<T extends unknown> = {
  data: Array<T>;
  columns: Array<Column<T>>;
  onClick: (data: T) => void;
  reference: (node?: Element | null | undefined) => void;
  hasNextPage: boolean;
  total: number;
};

export const TableBody = <T extends unknown>({
  data,
  columns,
  onClick,
  reference,
  hasNextPage,
  total,
}: TableBodyProps<T>): React.ReactElement => {
  const components = (field: T) => {
    return columns.map((column) => {
      return (
        <Fragment key={uuid()}>
          <TableCell field={field} column={column} key={column.expresion} />
        </Fragment>
      );
    });
  };

  return (
    <MUITableBody>
      {data.map((field) => (
        <TableRow key={uuid()} hover onClick={() => onClick(field)}>
          {components(field)}
        </TableRow>
      ))}
      {hasNextPage && (
        <TableRow ref={reference}>
          <MUITableCell colSpan={total}>
            <div className="w-full h-full flex justify-center items-center">
              <CircularProgress size={'20px'}></CircularProgress>
            </div>
          </MUITableCell>
        </TableRow>
      )}
    </MUITableBody>
  );
};
