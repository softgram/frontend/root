'use client';

import { Table as MUITable } from '@mui/material';
import { TableBody } from './table-body';
import {
  FetchNextPageOptions,
  InfiniteData,
  InfiniteQueryObserverResult,
} from '@tanstack/react-query';
import { useInView } from 'react-intersection-observer';
import { TableHead } from './table-head';
import * as React from 'react';

type Header = {
  text: string;
  className?: string;
  component?: React.FC<{ text: string }>;
};

type Column<T extends unknown> = {
  expresion: string;
  component?: React.FC<{ data: string; all?: T }>;
  className?: string;
};

type TableProps<T extends unknown> = {
  headers: Array<Header>;
  columns: Array<Column<T>>;
  onClick: (data: T) => void;
  data?: InfiniteData<Array<T>>;
  fetchNextPage: (
    options?: FetchNextPageOptions | undefined,
  ) => Promise<InfiniteQueryObserverResult<any, unknown>>;
  hasNextPage: boolean;
  stickyHeader?: boolean;
};

const Table = <T extends unknown>({
  data,
  headers,
  columns,
  onClick,
  fetchNextPage,
  hasNextPage,
  stickyHeader = true,
}: TableProps<T>): React.ReactElement => {
  const { ref, entry } = useInView({
    threshold: 0,
  });

  const pages = React.useMemo(
    () => data?.pages.reduce((prevPages, page) => prevPages.concat(page)),
    [data],
  );

  if (entry?.isIntersecting && hasNextPage) {
    fetchNextPage();
  }

  if (!pages || pages === null) throw new Error('');

  return (
    <MUITable stickyHeader={stickyHeader}>
      <TableHead headers={headers} />
      <TableBody<T>
        data={pages}
        columns={columns}
        onClick={onClick}
        reference={ref}
        hasNextPage={hasNextPage}
        total={headers.length}
      />
    </MUITable>
  );
};

export type { TableProps, Header, Column };
export { Table as InfinityTable };
