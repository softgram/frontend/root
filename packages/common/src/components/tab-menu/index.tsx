'use client';

import React from 'react';
import { Tabs, Tab, SxProps, Theme, Box } from '@mui/material';
import { useNavigate } from 'react-router-dom';

type TabType = {
  to: string;
  label: string;
};

type TabMenuProps = {
  tabs: Array<TabType>;
  className?: string;
  sx?: SxProps<Theme>;
};

export const TabMenu = ({
  tabs,
  className,
  sx,
}: TabMenuProps): React.ReactElement => {
  const navigate = useNavigate();

  const [value, setValue] = React.useState(0);

  const handleChange = (_: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  const handleClick = (to: string) => {
    navigate(to);
  };

  return (
    <Box sx={{ borderBottom: 1, borderColor: 'divider' }} className={className}>
      <Tabs sx={sx} onChange={handleChange} value={value}>
        {tabs.map((tab) => (
          <Tab
            label={tab.label}
            onClick={() => handleClick(tab.to)}
            sx={{
              fontSize: '12px',
            }}
            key={tab.to}
          ></Tab>
        ))}
      </Tabs>
    </Box>
  );
};
