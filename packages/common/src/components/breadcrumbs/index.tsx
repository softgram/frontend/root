'use client';

import React from 'react';
import { Breadcrumbs as MUIBreadcrumbs, Typography, Link } from '@mui/material';
import { useLocation } from 'react-router';

export const Breadcrumbs = (): React.ReactElement => {
  const { pathname } = useLocation();
  return (
    <MUIBreadcrumbs>
      {pathname
        .split('/')
        .filter((path) => {
          const data = parseInt(path);
          if (isNaN(data)) {
            return true;
          } else {
            return false;
          }
        })
        .slice(
          1,
          pathname.split('/').filter((path) => {
            const data = parseInt(path);
            if (isNaN(data)) {
              return true;
            } else {
              return false;
            }
          }).length - 1,
        )
        .map((path) => (
          <Link sx={{ fontSize: '13px' }} key={path}>
            {path}
          </Link>
        ))}
      <Typography
        sx={{
          fontSize: '13px',
        }}
      >
        {
          pathname.split('/').filter((path) => {
            const data = parseInt(path);
            if (isNaN(data)) {
              return true;
            } else {
              return false;
            }
          })[
            pathname.split('/').filter((path) => {
              const data = parseInt(path);
              if (isNaN(data)) {
                return true;
              } else {
                return false;
              }
            }).length - 1
          ]
        }
      </Typography>
    </MUIBreadcrumbs>
  );
};
