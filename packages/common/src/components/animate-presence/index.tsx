'use client';

import React from 'react';
import {
  AnimatePresence as FMAnimatePresence,
  motion,
  Variants,
} from 'framer-motion';

type AnimatePresenceProps = {
  children: React.ReactNode;
  className?: string;
  x?: number;
  y?: number;
  duration: number;
  delay?: number;
};

const AnimatePresence = ({
  children,
  className,
  x,
  y,
  duration,
  delay = 0,
}: AnimatePresenceProps): React.ReactElement => {
  const variants: Variants = {
    hidden: {
      opacity: 0,
      x,
      y,
    },
    show: {
      opacity: 1,
      x: 0,
      y: 0,
      transition: {
        duration,
        delay,
      },
    },
    exit: {
      opacity: 0,
      x: (x ?? 100) * -1,
      y: (y ?? 100) * -1,
      transition: {
        duration: duration * 2,
        delay,
      },
    },
  };

  return (
    <FMAnimatePresence>
      <motion.div
        variants={variants}
        initial="hidden"
        animate="show"
        exit={'exit'}
        className={className}
      >
        {children}
      </motion.div>
    </FMAnimatePresence>
  );
};

const AnimatePresenceExpresion = ({
  children,
  className,
  x,
  y,
  duration,
  delay = 0,
  exitExpresion,
}: AnimatePresenceProps & { exitExpresion: boolean }): React.ReactElement => {
  const variants: Variants = {
    hidden: {
      opacity: 0,
      x,
      y,
    },
    show: {
      opacity: 1,
      x: 0,
      y: 0,
      transition: {
        duration,
        delay,
      },
    },
    exit: {
      opacity: 0,
      x: (x ?? 0) * -1,
      y: (y ?? 0) * -1,
      transition: {
        duration: duration * 2,
        delay,
      },
    },
  };

  return (
    <FMAnimatePresence>
      {exitExpresion && (
        <motion.div
          variants={variants}
          initial="hidden"
          animate="show"
          exit={'exit'}
          className={className}
        >
          {children}
        </motion.div>
      )}
    </FMAnimatePresence>
  );
};
export { AnimatePresence, AnimatePresenceExpresion };
