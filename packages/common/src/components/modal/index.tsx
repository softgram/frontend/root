'use client';

import { Dialog, DialogTitle, DialogContent, PaperProps } from '@mui/material';

const PaperComponent = ({ children }: PaperProps) => {
  return <div>{children}</div>;
};

type ModalProps = {
  open: boolean;
  onClose: () => void;
  title?: string;
  children?: React.ReactNode;
};

export const Modal = ({
  open,
  onClose,
  title,
  children,
}: ModalProps): React.ReactElement => {
  return (
    <Dialog open={open} onClose={onClose} PaperComponent={PaperComponent}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>{children}</DialogContent>
    </Dialog>
  );
};
