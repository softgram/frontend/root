'use client';

import { useState } from 'react';

type ReturnValue = {
  page: number;
  onNext: () => void;
  onBack: () => void;
};

export const useStepper = (initial: number): ReturnValue => {
  const [page, setPage] = useState<number>(initial);

  const onNext = () => {
    setPage((prev) => prev + 1);
  };

  const onBack = () => {
    setPage((prev) => prev - 1);
  };

  return { page, onNext, onBack };
};
