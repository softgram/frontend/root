'use client';

import {
  Stepper as MUIStepper,
  Step as MUIStep,
  StepLabel,
} from '@mui/material';
import { StepType } from '.';

export const StepperPointer = <T extends Record<string, unknown>>({
  steps,
  page,
}: {
  page: number;
  steps: Array<StepType<T>>;
}): React.ReactElement => {
  return (
    <MUIStepper activeStep={page}>
      {steps.map((step) => (
        <MUIStep key={step.name}>
          <StepLabel>{step.name}</StepLabel>
        </MUIStep>
      ))}
    </MUIStepper>
  );
};
