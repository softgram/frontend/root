'use client';

import { Form, FormProps } from '../forms/form';
import { FC, Fragment, useMemo } from 'react';
import { SubmitHandler, UseFormReturn } from 'react-hook-form';
import { AnyObjectSchema } from 'yup';
import { Step } from './step';
import { useStepper } from './use-stepper';
import { StepperPointer } from './pointer';

type StepType<T extends Record<string, unknown>> = {
  name: string;
  component: FC<{
    onBack?: () => void;
    onNext?: () => void;
    methods?: UseFormReturn<T, unknown>;
  }>;
};

type StepperFormProps<T extends Record<string, unknown>> = Omit<
  FormProps<T>,
  'children' | 'schema' | 'onSubmit'
> & {
  onSubmit: (onNext: () => void) => SubmitHandler<T>;
  steps: Array<StepType<T>>;
  schema?: Array<AnyObjectSchema | undefined>;
  pointer?: boolean;
};

const StepperFormComponent = <T extends Record<string, unknown>>({
  onSubmit,
  steps,
  className,
  schema,
  pointer = false,
}: StepperFormProps<T>): React.ReactElement => {
  const { page, onNext, onBack } = useStepper(0);
  const Component = useMemo(() => steps[page].component, [page]);

  return (
    <Form<T>
      onSubmit={onSubmit(onNext)}
      schema={schema?.[page]}
      className={className}
    >
      {(methods) => {
        return (
          <Fragment>
            {pointer && <StepperPointer steps={steps} page={page} />}
            <Component methods={methods} onNext={onNext} onBack={onBack} />
          </Fragment>
        );
      }}
    </Form>
  );
};

StepperFormComponent.Step = Step;

export type { StepType };
export { StepperFormComponent as Stepper };
