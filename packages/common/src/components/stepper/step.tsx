'use client';

import { AnimatePresence, Variants, motion } from 'framer-motion';
import React from 'react';

type StepProps = {
  children: React.ReactNode;
  className?: string;
};

const variants: Variants = {
  hidden: {
    opacity: 0,
    x: 500,
  },
  show: {
    opacity: 1,
    x: 0,
    transition: {
      duration: 0.7,
    },
  },
};

export const Step = ({
  children,
  className,
}: StepProps): React.ReactElement => {
  return (
    <AnimatePresence>
      <motion.div
        animate="show"
        initial="hidden"
        variants={variants}
        className={className}
      >
        {children}
      </motion.div>
    </AnimatePresence>
  );
};
