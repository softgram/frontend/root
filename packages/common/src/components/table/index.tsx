'use client';

import { Table as MUITable } from '@mui/material';
import { TableBody } from './table-body';
import { TableHead } from './table-head';

type Header = {
  text: string;
  className?: string;
  component?: React.FC<{ text: string }>;
};

type Column<T extends unknown> = {
  expresion: string;
  component?: React.FC<{ data: string; all?: T }>;
  className?: string;
};

type TableProps<T extends unknown> = {
  data: Array<T>;
  headers: Array<Header>;
  columns: Array<Column<T>>;
  onClick?: (data: T) => void;
};

const Table = <T extends unknown>({
  data,
  headers,
  columns,
  onClick,
  stickyHeader = true,
}: TableProps<T> & {
  stickyHeader?: boolean;
  animate?: boolean;
}): React.ReactElement => {
  return (
    <MUITable stickyHeader={stickyHeader}>
      <TableHead headers={headers} />
      <TableBody<T> data={data} columns={columns} onClick={onClick} />
    </MUITable>
  );
};

export type { TableProps, Header, Column };
export { Table };
