import React from 'react';
import { TableHead as MUITableHead, TableRow, TableCell } from '@mui/material';
import { Header } from '.';

type TableHeadProps = {
  headers: Array<Header>;
};

export const TableHead = ({ headers }: TableHeadProps): React.ReactElement => {
  return (
    <MUITableHead>
      <TableRow>
        {headers.map((header) => (
          <TableCell key={header.text}>
            {header.component ? (
              <header.component text={header.text}></header.component>
            ) : (
              <div className={header.className}>{header.text}</div>
            )}
          </TableCell>
        ))}
      </TableRow>
    </MUITableHead>
  );
};
