'use client';

import {
  TableBody as MUITableBody,
  TableRow,
  TableCell as MUITableCell,
} from '@mui/material';
import { Column, TableProps } from '.';
import _ from 'lodash';

import { v4 as uuid } from 'uuid';
import { Fragment } from 'react';

const TableCell = <T extends unknown>({
  field,
  column,
}: {
  field: T;
  column: Column<T>;
}) => {
  return (
    <MUITableCell>
      {column.component ? (
        <column.component
          data={
            column.expresion === 'all' ? '' : _.get(field, column.expresion)
          }
          all={
            column.expresion === 'all' ? field : _.get(field, column.expresion)
          }
        ></column.component>
      ) : (
        <div className={column.className}>{_.get(field, column.expresion)}</div>
      )}
    </MUITableCell>
  );
};

export const TableBody = <T extends unknown>({
  data,
  columns,
  onClick,
}: Omit<TableProps<T>, 'headers' | 'onClick'> & {
  onClick?: (data: T) => void;
}): React.ReactElement => {
  const components = (field: T) => {
    return columns.map((column) => {
      return (
        <Fragment key={uuid()}>
          <TableCell field={field} column={column} key={column.expresion} />
        </Fragment>
      );
    });
  };

  return (
    <MUITableBody>
      {data.map((field) =>
        onClick ? (
          <TableRow key={uuid()} hover onClick={() => onClick(field)}>
            {components(field)}
          </TableRow>
        ) : (
          <TableRow key={uuid()} hover>
            {components(field)}
          </TableRow>
        ),
      )}
    </MUITableBody>
  );
};
